package com.example.shoppingcartmvvm.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shoppingcartmvvm.databinding.ShopRowBinding;
import com.example.shoppingcartmvvm.models.Product;

public class ShopListAdapter extends ListAdapter<Product,ShopListAdapter.shopViewHolder> {


    //declare interface variable
    ShopInterface shopInterface;

//constructor class
    //initialize shopinterface in shop list adapater
    public ShopListAdapter(ShopInterface shopInterface) {
        super(Product.itemCallback);
        this.shopInterface = shopInterface;
    }

    //constructor methods for viewholders

    @NonNull
    @Override
    public shopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ShopRowBinding shopRowBinding = ShopRowBinding.inflate(layoutInflater,parent,false);

        //set shop interface on shopRowBinding
        shopRowBinding.setShopInterface(shopInterface);
        return new shopViewHolder(shopRowBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull shopViewHolder holder, int position) {

        //setting data with data  binding
        Product product =getItem(position);
        holder.shopRowBinding.setProduct(product);
        holder.shopRowBinding.executePendingBindings();
    }

    //shop view holder extending RecycyclerView.Viewholder
    class shopViewHolder extends RecyclerView.ViewHolder
    {

        //fixing shopRowBinding here
        ShopRowBinding shopRowBinding;
        public shopViewHolder(ShopRowBinding binding) {
            super(binding.getRoot());

            this.shopRowBinding = binding;
        }
    }


    //create Interface

    public interface ShopInterface
    {
        void addItem(Product product);
        void onItemclick(Product product);

    }

}
