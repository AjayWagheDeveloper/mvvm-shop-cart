package com.example.shoppingcartmvvm.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.shoppingcartmvvm.R;
import com.example.shoppingcartmvvm.models.CartItem;
import com.example.shoppingcartmvvm.viewmodels.ShopViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    
    
    //app bar tile change via navController
    NavController navController;

    //shopview model
    ShopViewModel shopViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //setting labels -titles of appbar using navCOntroller
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        NavigationUI.setupActionBarWithNavController(this, navController);

        //init shop view model
        shopViewModel = new ViewModelProvider(this).get(ShopViewModel.class);

        //shopview model observer
        shopViewModel.getCart().observe(this, new Observer<List<CartItem>>() {
            @Override
            public void onChanged(List<CartItem> cartItems) {

                Log.d(TAG, "onChanged: "+ cartItems.size());
            }
        });



    }

    //inflate menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu,menu);
        return true;
    }

    //click listner for menu


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        //nav ui for selection
        NavigationUI.onNavDestinationSelected(item,navController);

        return super.onOptionsItemSelected(item);
    }

    //navigiation up arrow Appbar click

    @Override
    public boolean onSupportNavigateUp() {
        navController.navigateUp();
        return super.onSupportNavigateUp();
    }
}