package com.example.shoppingcartmvvm.views;

import android.content.Context;
import android.icu.lang.UCharacter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.shoppingcartmvvm.R;
import com.example.shoppingcartmvvm.adapters.ShopListAdapter;
import com.example.shoppingcartmvvm.databinding.FragmentShopBinding;
import com.example.shoppingcartmvvm.models.Product;
import com.example.shoppingcartmvvm.viewmodels.ShopViewModel;

import java.util.List;


public class ShopFragment extends Fragment implements ShopListAdapter.ShopInterface {


    private static final String TAG = "ShopFragment";
    //initializing fragment shop binding as follows
    FragmentShopBinding fragmentShopBinding;

    private ShopListAdapter shopListAdapter;

    private ShopViewModel shopViewModel;

    //nav controller
    private NavController navController;


    

    public ShopFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        //we are using data binding here so we commented line below
        //return inflater.inflate(R.layout.fragment_shop, container, false);

        fragmentShopBinding = FragmentShopBinding.inflate(inflater,container,false);

        return fragmentShopBinding.getRoot();


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //passing shopInteface in constructor . this bcause it is implemented in ShopFragment
        shopListAdapter = new ShopListAdapter(this);

        fragmentShopBinding.shopRecyclerView.setAdapter(shopListAdapter);

        //adding divider to recyclerview
        fragmentShopBinding.shopRecyclerView.addItemDecoration(new DividerItemDecoration(requireContext(),DividerItemDecoration.VERTICAL));
        fragmentShopBinding.shopRecyclerView.addItemDecoration(new DividerItemDecoration(requireContext(),DividerItemDecoration.HORIZONTAL));


        shopViewModel = new ViewModelProvider(requireActivity()).get(ShopViewModel.class);
        shopViewModel.getproducts().observe(getViewLifecycleOwner(), new Observer<List<Product>>() {
            @Override
            public void onChanged(List<Product> products) {

                shopListAdapter.submitList(products);
            }
        });

        navController = Navigation.findNavController(view);
    }



    //interface methods

    @Override
    public void addItem(Product product) {

        //Log.d(TAG,"add item" +product.toString());

        boolean isAdded = shopViewModel.addItemToCart(product);

        Log.d(TAG, "\nis Added : "+ product.getName() + isAdded);
    }

    @Override
    public void onItemclick(Product product) {

        Toast.makeText(getContext(), "clicked.."+product.getName(), Toast.LENGTH_SHORT).show();

        shopViewModel.setProduct(product);

        navController.navigate(R.id.action_shopFragment_to_productDetailFragment);
    }
}