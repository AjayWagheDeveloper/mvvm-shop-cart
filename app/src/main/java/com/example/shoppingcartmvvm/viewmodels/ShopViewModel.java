package com.example.shoppingcartmvvm.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.shoppingcartmvvm.models.CartItem;
import com.example.shoppingcartmvvm.models.Product;
import com.example.shoppingcartmvvm.repositories.CartRepo;
import com.example.shoppingcartmvvm.repositories.ShopRepo;

import java.util.List;

public class ShopViewModel extends ViewModel {


    //shop repo
    ShopRepo shopRepo = new ShopRepo();

    //cart repo
    CartRepo cartRepo = new CartRepo();

    //mutabale live data for product
    MutableLiveData<Product> mutableProduct = new MutableLiveData<>();

    public LiveData<List<Product>> getproducts() {
        return shopRepo.getProducts();
    }

    public void setProduct(Product product) {
        mutableProduct.setValue(product);

    }

    public LiveData<Product> getProduct() {

        return mutableProduct;
    }


    public LiveData<List<CartItem>> getCart()
    {
        return cartRepo.getCart();
    }

    //add product item to cart
    public boolean addItemToCart(Product product)
    {

        return cartRepo.addItemToCart(product);

    }
}
